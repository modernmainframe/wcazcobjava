package com.ibm.wca4z;

import com.ibm.jzos.fields.*;
import java.sql.*; 
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Arrays;
import org.json.simple.*;
import org.json.simple.parser.*;
import java.io.FileReader; 
import java.io.*;
import org.json.simple.parser.ParseException;

public class CBSBSDG implements Comparable<CBSBSDG> {
    // Start of COBOL-compatible binary serialization metadata
    private static CobolDatatypeFactory factory = new CobolDatatypeFactory();
    static {
        factory.setStringTrimDefault(true);
        factory.setStringEncoding("IBM-1047");
    }
    
    protected static final int SIZE = factory.getOffset();
    // End of COBOL-compatible binary serialization metadata
    public CBSBSDG() {
    }
    
    public CBSBSDG(CBSBSDG that) {
    }
    
    protected CBSBSDG(byte[] bytes, int offset) {
        setBytes(bytes, offset);
    }
    
    protected CBSBSDG(byte[] bytes) {
        this(bytes, 0);
    }
    
    public static CBSBSDG fromBytes(byte[] bytes, int offset) {
        return new CBSBSDG(bytes, offset);
    }
    
    public static CBSBSDG fromBytes(byte[] bytes) {
        return fromBytes(bytes, 0);
    }
    
    public static CBSBSDG fromBytes(String bytes) {
        try {
            return fromBytes(bytes.getBytes(factory.getStringEncoding()));
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
    }
    
    public static void deregAcctStats(String[] args) throws SQLException {
    Csrgres csrgres = new Csrgres();
    DclcbsAcctMstrDtl dclcbsAcctMstrDtl = new DclcbsAcctMstrDtl();
    String wkInactive = "INACTIVE";
     
    // edited part start
    String h1AccountName = "Olivia";
    int h1CustomerId = 1;
    String h1AccountNumber = "100000001001";
    // edited part end
    
    csrgres.setCustomerName(h1AccountName);
    csrgres.setCustomerId(h1CustomerId);
    System.out.println("DEREGISTER PARA");
    
    // edited part start
//    try {
//    	 
//        String sql = "UPDATE CBS_ACCT_MSTR_DTL SET ACCOUNT_STATUS = ? WHERE ACCOUNT_NUMBER = ?";
//        PreparedStatement ps = JdbcConnection.connection.prepareStatement(sql);
//        ps.setString(1, wkInactive);
//        ps.setLong(2, h1AccountNumber);
//        ps.executeUpdate();
//        ps.close();
//    }
//    catch(SQLException exception){
//        System.out.println(exception);
//        return;
//    }
    
    String url = "";
    String user = "";
    String password = "";
    
    try {
        JSONParser parser = new JSONParser();
        //Use JSONObject for simple JSON and JSONArray for array of JSON.
        JSONObject data = (JSONObject) parser.parse(
              new FileReader("/u/ibmuser/temp1/mvnArtifact/inputArgs.json"));//path to the JSON file.

        // Fetch the values using appropriate methods for their data types
        url = (String) data.get("url");
        user = (String) data.get("name");
        password = (String) data.get("pswd");

        String json = data.toJSONString();
    } catch (IOException | ParseException e) {
        e.printStackTrace();
    }
    
    Connection con;
    try 
    {                                                                        
      // Load the driver
      Class.forName("com.ibm.db2.jcc.DB2Driver");                              
      System.out.println("**** Loaded the JDBC driver");

      // Create the connection using the IBM Data Server Driver for JDBC and SQLJ
      con = DriverManager.getConnection (url, user, password);                 
      // Commit changes manually
      con.setAutoCommit(false);
      System.out.println("**** Created a JDBC connection to the data source");


      // Execute a query and generate a ResultSet instance
      String sql = "UPDATE COREBK.CBS_ACCT_MSTR_DTL SET ACCOUNT_STATUS = ? WHERE ACCOUNT_NUMBER = ?";
      PreparedStatement ps = con.prepareStatement(sql);
      ps.setString(1, wkInactive);
      ps.setString(2, h1AccountNumber);
      ps.executeUpdate();
      ps.close();
                   
      System.out.println("**** Created JDBC ResultSet object");

      // Connection must be on a unit-of-work boundary to allow close
      con.commit();
      System.out.println ( "**** Transaction committed" );
      
      // Close the connection
      con.close();                                                            
      System.out.println("**** Disconnected from data source");

      System.out.println("**** JDBC Exit from class CBSBSDG - no errors");
      
      System.out.println("**** Customer is derigestered");

    } catch (ClassNotFoundException e) {
      System.err.println("Could not load JDBC driver");
      System.out.println("Exception: " + e);
//      e.printStackTrace();
    }
    // edited part end
    csrgres.setMessages("CUSTOMER DEREGISTERED SUCESSFULLY");
}


    
//    public static void(String[] args) {}
    
    // public static void deregAcctStats(String[] args) {}
    
    // edited part start
    public static void main(String[] args) throws SQLException {
    	deregAcctStats(args);
    }
    // edited part end
    public String toString() {
        StringBuilder s = new StringBuilder();
        s.append("\" }");
        return s.toString();
    }
    
    public boolean equals(CBSBSDG that) {
        return true;
    }
    
    @Override
    public boolean equals(Object that) {
        return (that instanceof CBSBSDG) && this.equals((CBSBSDG)that);
    }
    
    @Override
    public int hashCode() {
        return 0;
    }
    
    @Override
    public int compareTo(CBSBSDG that) {
        int c = 0;
        return c;
    }
    
    public byte[] getBytes(byte[] bytes, int offset) {
        return bytes;
    }
    
    public final byte[] getBytes(byte[] bytes) {
        return getBytes(bytes, 0);
    }
    
    public final byte[] getBytes() {
        return getBytes(new byte[numBytes()]);
    }
    
    public final String toByteString() {
        try {
            return new String(getBytes(), factory.getStringEncoding());
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
    }
    
    public void setBytes(byte[] bytes, int offset) {
        if (bytes.length < SIZE + offset) {
            byte[] newBytes = Arrays.copyOf(bytes, SIZE + offset);
            Arrays.fill(newBytes, bytes.length, SIZE + offset, (byte)0x40 /*default EBCDIC space character*/);
            bytes = newBytes;
        }
    }
    
    public final void setBytes(byte[] bytes) {
        setBytes(bytes, 0);
    }
    
    public final void setBytes(String bytes) {
        try {
            setBytes(bytes.getBytes(factory.getStringEncoding()));
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
    }
    
    public int numBytes() {
        return SIZE;
    }
}

